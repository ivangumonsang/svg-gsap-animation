const hamburger = $('.hamburger')
const line1 = $('.line1')
const line2 = $('.line2')
const line3 = $('.line3')

const line = [line1, line2, line3]

const tlm = new TimelineMax({})
const toggleMenu = new TimelineMax({paused: true, reversed: true})

hamburger.mouseenter( ()=>{
    if($('.hamburger').hasClass('js-x')){
        return;
    }
    tlm.staggerTo(line, 0.25, {scaleX: 1.5, repeat: 1, yoyo: true, svgOrigin: "50 50"}, 0.35)
})
toggleMenu
    .to(line2, 0.125, {scaleX: 0}, 0)
    .to(line1, 0.25, { transformOrigin:"50% 50%", y: 8, ease: Power2.easeInOut}, "slide")
    .to(line3, 0.25, {transformOrigin:"50% 50%", y:-8, ease: Power2.easeInOut}, "slide")
    .to(hamburger, 0.25, {rotation: 360})
    .to(line1, 0.25, {rotation:45, transformOrigin:"50% 50%", y: 8, ease: Power2.easeInOut}, "eman")
    .to(line3, 0.25, {rotation:-45,  transformOrigin:"50% 50%", y:-8, ease: Power2.easeInOut}, "eman")

hamburger.click( () =>{
    hamburger.toggleClass('js-x')
    toggleMenu.reversed() ? toggleMenu.play() : toggleMenu.reverse()
})