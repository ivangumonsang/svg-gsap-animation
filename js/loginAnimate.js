const emailLabel = $('#loginEmailLabel')
const email = $('#loginEmail')
const passwordLabel = $('#loginPasswordLabel')
const password = $('#loginPassword')
const showPasswordCheck = $('#showPasswordCheck')
const showPasswordToggle = $('#showPasswordToggle')
const svgContainer = $('#svgContainer')

const bgLines = $('#bgLines')
const leftEye = $('#eyeLeft')
const rightEye = $('#eyeRight')
const leftPupil = $('#pupil')
const rightPupil = $('#pupil-2')

const biguteLeft = $('#LeftMushtache')
const biguteRight = $('#rightMushtache')
const smile = $('#smile')
const normalSmile = $('#normalSmile')
const leftArm = $('#leftArm')
const rightArm = $('#rightarm')
const tlm = new TimelineMax({})

email.focus( ()=>{
    tlm.to(leftPupil, 0.5, {y: 30, x: -10}, "eyes")
    tlm.to(rightPupil, 0.5 , {y: 30, x: -10}, "eyes")
    smile.show()
})

email.blur(()=>{
    TweenMax.to(leftPupil, 0.5, {y: 0, x: 0})
    TweenMax.to(rightPupil, 0.5, {y: 0, x: 0})
    smile.hide()
})

email.keydown( ()=>{
    let value = email.val()
    let valueLen = value.length
    let movePupil = -10 + valueLen
    TweenMax.to(leftPupil, 0.5, {y: 30, x: movePupil})
    TweenMax.to(rightPupil, 0.5, {y: 30, x: movePupil})
})

password.focus(()=>{
    TweenMax.to(leftArm, 0.5, {y:0})
    TweenMax.to(rightArm, 0.5, {y:0})
    leftArm.show()
    rightArm.show()
    // smile.hide()
}).blur(()=>{
    TweenMax.to(leftArm, 0.5, {y:500})
    TweenMax.to(rightArm, 0.5, {y:500})
})